import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_list_movies/models/movie_model.dart';
import 'package:flutter_app_list_movies/pages/home_page_movie.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

class addmovies extends StatefulWidget {
  const addmovies({Key? key}) : super(key: key);

  @override
  _addmoviesState createState() => _addmoviesState();
}

class _addmoviesState extends State<addmovies> {
  //final movieData data = movieData();

  final data = GetIt.I.get<movieData>();

  @override
  Widget build(BuildContext context) {
    var _chosenTags;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "New Movie",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        "Save",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 24,
                        ),
                      ),
                      onTap: () {
                        // data.save_movie_data();
                        Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) => viewListMovie()),
                        );
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 35,
              ),
              Observer(
                builder: (_) => TextField(
                  onChanged: (value) => data.tittle = value,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Tittle',
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Observer(
                builder: (_) => TextField(
                  onChanged: (value) => data.director = value,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Director',
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Observer(
                builder: (_) => Container(
                  child: DropdownButtonFormField<String>(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                    ),
                    value: _chosenTags,
                    //elevation: 5,
                    style: TextStyle(color: Colors.black),

                    items: <String>[
                      'Action',
                      'Comedy',
                      'Fantasy',
                      'Horror',
                      'Sci-F'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    hint: Container(
                      child: Text(
                        "Tags",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    onChanged: (String? newValue) {
                      setState(() {
                        _chosenTags = newValue!;
                        data.tags = _chosenTags;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Observer(
                builder: (_) => TextField(
                  onChanged: (value) => data.summary = value,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Summary',
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
