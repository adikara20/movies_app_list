import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_list_movies/models/movie_model.dart';
import 'package:flutter_app_list_movies/pages/home_page_movie.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

class editmovies extends StatefulWidget {
  const editmovies({Key? key}) : super(key: key);

  @override
  _editmoviesState createState() => _editmoviesState();
}

class _editmoviesState extends State<editmovies> {
  //final movieData data = movieData();

  final data = GetIt.I.get<movieData>();

  @override
  Widget build(BuildContext context) {
    var _chosenTags = data.tags;
    TextEditingController myController = TextEditingController()
      ..text = data.tittle;
    TextEditingController myController2 = TextEditingController()
      ..text = data.director;
    // TextEditingController myController3 = TextEditingController()
    //   ..text = data.tags;
    TextEditingController myController4 = TextEditingController()
      ..text = data.summary;

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Edit Movie",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                      ),
                    ),
                    Row(
                      children: [
                        GestureDetector(
                          child: Text(
                            "Update",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 24,
                            ),
                          ),
                          onTap: () {
                            // data.save_movie_data();
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => viewListMovie()),
                            );
                          },
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        GestureDetector(
                          child: Text(
                            "Delete",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 24,
                            ),
                          ),
                          onTap: () {
                            data.tittle = '';
                            data.director = '';
                            data.tags = '';
                            data.summary = '';
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => viewListMovie()),
                            );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 35,
              ),
              Observer(
                builder: (_) => TextField(
                  controller: myController,
                  onChanged: (value) => data.tittle = value,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Tittle',
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Observer(
                builder: (_) => TextField(
                  controller: myController2,
                  onChanged: (value) => data.director = value,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Director',
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Observer(
                builder: (_) => Container(
                  child: DropdownButtonFormField<String>(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                    ),
                    value: _chosenTags,
                    //elevation: 5,
                    style: TextStyle(color: Colors.black),

                    items: <String>[
                      'Action',
                      'Comedy',
                      'Fantasy',
                      'Horror',
                      'Sci-F'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    hint: Container(
                      child: Text(
                        "Tags",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    onChanged: (String? newValue) {
                      setState(() {
                        _chosenTags = newValue!;
                        data.tags = _chosenTags;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Observer(
                builder: (_) => TextField(
                  controller: myController4,
                  onChanged: (value) => data.summary = value,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Summary',
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
