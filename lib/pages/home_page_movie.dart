import 'package:flutter/material.dart';
import 'package:flutter_app_list_movies/models/movie_model.dart';
import 'package:flutter_app_list_movies/pages/add_movies.dart';
import 'package:flutter_app_list_movies/pages/edit_movies.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

class viewListMovie extends StatefulWidget {
  // const viewListMovie({Key? key}) : super(key: key);
  final bool = false;
  @override
  viewListMovieState createState() => viewListMovieState();
}

class viewListMovieState extends State<viewListMovie> {
  final data = GetIt.I.get<movieData>();
  // final movieData data = movieData();
  @override
  Widget build(BuildContext context) {
    var _chosenTags;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "List Movie",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        "Add",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 24,
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => addmovies()));
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 35,
              ),
              Expanded(child: Observer(
                builder: (_) {
                  return ListView.builder(
                    itemCount: 1,
                    itemBuilder: (_, index) {
                      return GestureDetector(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                            ),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  data.tittle,
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  data.director,
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  data.tags,
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  data.summary,
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => editmovies()),
                          );
                        },
                      );
                    },
                  );
                },
              )),
            ],
          ),
        ),
      ),
    );
  }
}
