import 'package:mobx/mobx.dart';
part 'movie_model.g.dart';

class movieData = _movieData with _$movieData;

abstract class _movieData with Store {
  @observable
  int id = 0;

  @observable
  String tittle = '';

  @observable
  String director = '';

  @observable
  String summary = '';

  @observable
  // List<String> tags = ['Action', 'Comedy', 'Fantasy', 'Horror', 'Sci-F'];
  String tags = '';

  @action
  void setId() {
    id++;
  }

  @action
  void setTittle(String value) {
    tittle = value;
  }

  @action
  void setDirector(String value) {
    director = value;
  }

  @action
  void setTags(String value) {
    tags = value;
  }

  @action
  void setSummary(String value) {
    summary = value;
  }

  @computed
  bool get isFormValid => tittle.isNotEmpty;

  @observable
  ObservableList<String> newtittle = ObservableList<String>();

  @action
  void addnewtittle() {
    print(newtittle);
    newtittle.add(tittle);
    print(newtittle);
  }

  // final DataErrorState error = DataErrorState();

  // @action
  // void validateTittle(String value) {
  //   error.tittle = value.isEmpty ? 'Cannot be blank' : null;
  // }

  // @action
  // void validateDirector(String value) {
  //   error.director = value.isEmpty ? 'Cannot be blank' : null;
  // }

  // @action
  // void validateAll() {
  //   validateTittle(tittle);
  //   validateDirector(director);
  // }
}


// class DataErrorState = _DataErrorState with _$DataErrorState;

// abstract class _DataErrorState with Store {
//   @observable
//   String? tittle;

//   @observable
//   String? director;

//   @computed
//   bool get hasErrors => tittle != null || director != null;
// }
