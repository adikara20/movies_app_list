// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$movieData on _movieData, Store {
  Computed<bool>? _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid,
              name: '_movieData.isFormValid'))
          .value;

  final _$idAtom = Atom(name: '_movieData.id');

  @override
  int get id {
    _$idAtom.reportRead();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.reportWrite(value, super.id, () {
      super.id = value;
    });
  }

  final _$tittleAtom = Atom(name: '_movieData.tittle');

  @override
  String get tittle {
    _$tittleAtom.reportRead();
    return super.tittle;
  }

  @override
  set tittle(String value) {
    _$tittleAtom.reportWrite(value, super.tittle, () {
      super.tittle = value;
    });
  }

  final _$directorAtom = Atom(name: '_movieData.director');

  @override
  String get director {
    _$directorAtom.reportRead();
    return super.director;
  }

  @override
  set director(String value) {
    _$directorAtom.reportWrite(value, super.director, () {
      super.director = value;
    });
  }

  final _$summaryAtom = Atom(name: '_movieData.summary');

  @override
  String get summary {
    _$summaryAtom.reportRead();
    return super.summary;
  }

  @override
  set summary(String value) {
    _$summaryAtom.reportWrite(value, super.summary, () {
      super.summary = value;
    });
  }

  final _$tagsAtom = Atom(name: '_movieData.tags');

  @override
  String get tags {
    _$tagsAtom.reportRead();
    return super.tags;
  }

  @override
  set tags(String value) {
    _$tagsAtom.reportWrite(value, super.tags, () {
      super.tags = value;
    });
  }

  final _$newtittleAtom = Atom(name: '_movieData.newtittle');

  @override
  ObservableList<String> get newtittle {
    _$newtittleAtom.reportRead();
    return super.newtittle;
  }

  @override
  set newtittle(ObservableList<String> value) {
    _$newtittleAtom.reportWrite(value, super.newtittle, () {
      super.newtittle = value;
    });
  }

  final _$_movieDataActionController = ActionController(name: '_movieData');

  @override
  void setId() {
    final _$actionInfo =
        _$_movieDataActionController.startAction(name: '_movieData.setId');
    try {
      return super.setId();
    } finally {
      _$_movieDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setTittle(String value) {
    final _$actionInfo =
        _$_movieDataActionController.startAction(name: '_movieData.setTittle');
    try {
      return super.setTittle(value);
    } finally {
      _$_movieDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDirector(String value) {
    final _$actionInfo = _$_movieDataActionController.startAction(
        name: '_movieData.setDirector');
    try {
      return super.setDirector(value);
    } finally {
      _$_movieDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setTags(String value) {
    final _$actionInfo =
        _$_movieDataActionController.startAction(name: '_movieData.setTags');
    try {
      return super.setTags(value);
    } finally {
      _$_movieDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setSummary(String value) {
    final _$actionInfo =
        _$_movieDataActionController.startAction(name: '_movieData.setSummary');
    try {
      return super.setSummary(value);
    } finally {
      _$_movieDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addnewtittle() {
    final _$actionInfo = _$_movieDataActionController.startAction(
        name: '_movieData.addnewtittle');
    try {
      return super.addnewtittle();
    } finally {
      _$_movieDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
id: ${id},
tittle: ${tittle},
director: ${director},
summary: ${summary},
tags: ${tags},
newtittle: ${newtittle},
isFormValid: ${isFormValid}
    ''';
  }
}
